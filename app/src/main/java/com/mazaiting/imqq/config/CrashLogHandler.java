package com.mazaiting.imqq.config;

import android.content.Context;
import android.util.Log;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UploadFileListener;
import com.mazaiting.report.AbstractCrashReportHandler;
import java.io.File;

/**
 * Created by mazaiting on 2017/9/13.
 */

public class CrashLogHandler extends AbstractCrashReportHandler {
  private static final String TAG = "MyHandler";
  CrashLogHandler(Context context) {
    super(context);
  }

  @Override protected void sendReport(String title, String body, File file) {
    Log.e(TAG, "sendReport: "+title);
    final BmobFile bmobFile = new BmobFile(file);
    bmobFile.uploadblock(new UploadFileListener() {
      @Override public void done(BmobException e) {
        if(e==null){
          //bmobFile.getFileUrl()--返回的上传文件的完整地址
          Log.e(TAG, "done: "+ "上传文件成功:" + bmobFile.getFileUrl());
        }else{
          Log.e(TAG, "done: "+ "上传文件失败：" + e.getMessage());
        }
      }
    });
  }
}
