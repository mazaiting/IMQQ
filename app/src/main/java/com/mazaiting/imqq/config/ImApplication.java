package com.mazaiting.imqq.config;

import android.app.Application;
import android.content.Context;
import cn.bmob.newim.BmobIM;
import com.mazaiting.imqq.presenter.message.MessageHandler;
import com.mazaiting.imqq.utils.UniversalImageLoader;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.squareup.leakcanary.LeakCanary;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * 项目的application
 * Created by mazaiting on 2017/9/4.
 */

public class ImApplication extends Application {
  private static Context mContext;
  @Override public void onCreate() {
    super.onCreate();
    mContext = this;
    //TODO 集成：1.8、初始化IM SDK，并注册消息接收器
    if (getApplicationInfo().packageName.equals(getMyProcessName())){
      BmobIM.init(this);
      BmobIM.registerDefaultMessageHandler(new MessageHandler(this));
    }
    //new CrashLogHandler(this);
    // 初始化内存检测工具
    LeakCanary.install(this);
    // 打印日志初始化
    Logger.addLogAdapter(new AndroidLogAdapter());
    // 图片加载初始化
    UniversalImageLoader.initImageLoader(this);
  }



  /**
   * 获取当前运行的进程名
   * @return
   */
  public static String getMyProcessName() {
    try {
      File file = new File("/proc/" + android.os.Process.myPid() + "/" + "cmdline");
      BufferedReader mBufferedReader = new BufferedReader(new FileReader(file));
      String processName = mBufferedReader.readLine().trim();
      mBufferedReader.close();
      return processName;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * 获取上下文
   * @return
   */
  public static Context getContext() {
    return mContext;
  }
}
