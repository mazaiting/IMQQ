package com.mazaiting.imqq.model.bean;

import android.content.Context;
import cn.bmob.newim.bean.BmobIMConversationType;
import java.io.Serializable;

/**
 * 对BmobIMConversation的抽象封装
 * Created by mazaiting on 2017/9/8.
 */

public abstract class Conversation implements Serializable, Comparable{
  /**会话ID*/
  protected String cId;
  /**会话类型*/
  protected BmobIMConversationType cType;
  /**会话名称*/
  protected String cName;
  /**获取头像--用于会话界面显示*/
  public abstract String getAvatar();
  /**获取最后一条消息的时间*/
  public abstract long getLastMessageTime();
  /**回去最后一条消息的内容*/
  public abstract String getLastMessageContent();
  /**获取未读会话个数*/
  public abstract int getUnReadCount();
  /**将所有消息标记为已读*/
  public abstract void readAllMessages();
  /**点击事件*/
  public abstract void onClick(Context context);
  /**长按事件*/
  public abstract void onLongClick(Context context);

  public String getName() {
    return cName;
  }

  public String getId(){
    return cId;
  }

  public BmobIMConversationType getType(){
    return cType;
  }

  @Override public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null || getClass() != obj.getClass()) return false;
    Conversation that = (Conversation) obj;
    if (!cId.equals(that.cId)) return false;
    return cType == that.cType;
  }

  @Override public int hashCode() {
    int result = cId.hashCode();
    result = 31 * result + cType.hashCode();
    return result;
  }

  @Override
  public int compareTo(Object another) {
    if (another instanceof Conversation){
      Conversation anotherConversation = (Conversation) another;
      long timeGap = anotherConversation.getLastMessageTime() - getLastMessageTime();
      if (timeGap > 0) return  1;
      else if (timeGap < 0) return -1;
      return 0;
    }else{
      throw new ClassCastException();
    }
  }
}
