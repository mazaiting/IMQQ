package com.mazaiting.imqq.model.bean;

import cn.bmob.v3.BmobObject;
import com.orhanobut.logger.Logger;

/**
 * 创建朋友表
 * Created by mazaiting on 2017/9/6.
 */

public class Friend extends BmobObject implements Comparable<Friend>{
  /**
   * 用户自己的信息
   */
  private UserInfo userInfo;
  /**
   * 用户朋友的信息
   */
  private UserInfo friendUserInfo;
  // 只在本地使用
  private transient String pinyin;

  public String getPinyin() {
    return pinyin;
  }

  public void setPinyin(String pinyin) {
    this.pinyin = pinyin;
  }

  public UserInfo getUserInfo() {
    return userInfo;
  }

  public void setUserInfo(UserInfo userInfo) {
    this.userInfo = userInfo;
  }

  public UserInfo getFriendUserInfo() {
    return friendUserInfo;
  }

  public void setFriendUserInfo(UserInfo friendUserInfo) {
    this.friendUserInfo = friendUserInfo;
  }

  @Override public int compareTo(Friend friend) {
    if ("D".equals(friend.getPinyin())){
      return 0;
    }
    if ("S".equals(friend.getPinyin())){
      return 1;
    }
    if ("W".equals(friend.getPinyin())){
      return 2;
    }
    return 0;
  }
}
