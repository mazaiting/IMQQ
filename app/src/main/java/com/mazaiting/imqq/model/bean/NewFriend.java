package com.mazaiting.imqq.model.bean;

import java.io.Serializable;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 新朋友
 * Created by mazaiting on 2017/9/6.
 */
@Entity
public class NewFriend implements Serializable{
  @Id
  private Long id;
  /**
   * 用户id
   */
  private String uid;
  /**
   * 消息内容
   */
  private String msg;
  /**
   * 姓名
   */
  private String name;
  /**
   * 头像
   */
  private String avatar;
  /**
   * 状态
   */
  private Integer status;
  /**
   * 时间
   */
  private Long time;

  public NewFriend(){}
  @Keep
  public NewFriend(Long id) {
    this.id = id;
  }
  @Keep
  public NewFriend(Long id, String uid, String msg, String name, String avatar, Integer status, Long time) {
    this.id = id;
    this.uid = uid;
    this.msg = msg;
    this.name = name;
    this.avatar = avatar;
    this.status = status;
    this.time = time;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Long getTime() {
    return time;
  }

  public void setTime(Long time) {
    this.time = time;
  }

  @Override public String toString() {
    return "NewFriend{" +
        "id=" + id +
        ", uid='" + uid + '\'' +
        ", msg='" + msg + '\'' +
        ", name='" + name + '\'' +
        ", avatar='" + avatar + '\'' +
        ", status=" + status +
        ", time=" + time +
        '}';
  }
}
