package com.mazaiting.imqq.model.bean;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import cn.bmob.newim.BmobIM;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMConversationType;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMMessageType;
import cn.bmob.newim.bean.BmobIMSendStatus;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.activity.message.ChatActivity;
import java.util.List;
import org.w3c.dom.Text;

/**
 * Created by mazaiting on 2017/9/8.
 */

public class PrivateConversation extends Conversation {
  private BmobIMConversation mConversation;
  private BmobIMMessage mMessage;

  public PrivateConversation(BmobIMConversation conversation) {
    this.mConversation = conversation;
    // 设置聊天类型
    cType = BmobIMConversationType.setValue(conversation.getConversationType());
    if (cType == BmobIMConversationType.PRIVATE) {
      cName = conversation.getConversationTitle();
      if (TextUtils.isEmpty(cName)) cName = cId;
    } else {
      cName = "位置会话";
    }
    List<BmobIMMessage> messages = conversation.getMessages();
    if (null != messages && messages.size() > 0) {
      mMessage = messages.get(0);
    }
  }

  @Override public String getAvatar() {
    if (cType == BmobIMConversationType.PRIVATE) {
      String avatar = mConversation.getConversationIcon();
      // 头像为空，使用默认头像
      if (TextUtils.isEmpty(avatar)) {
        return null;
      } else {
        return avatar;
      }
    } else {
      return null;
    }
  }

  @Override public long getLastMessageTime() {
    if (null != mMessage){
      return mMessage.getCreateTime();
    }else {
      return 0;
    }
  }

  @Override public String getLastMessageContent() {
    if (null != mMessage) {
      String content = mMessage.getContent();
      if (mMessage.getMsgType().equals(BmobIMMessageType.TEXT.getType()) || mMessage.getMsgType()
          .equals("agree")) {
        return content;
      } else if (mMessage.getMsgType().equals(BmobIMMessageType.IMAGE.getType())) {
        return "[图片]";
      } else if (mMessage.getMsgType().equals(BmobIMMessageType.VOICE.getType())) {
        return "[语音]";
      } else if (mMessage.getMsgType().equals(BmobIMMessageType.LOCATION.getType())) {
        return "[位置]";
      } else if (mMessage.getMsgType().equals(BmobIMMessageType.VIDEO.getType())) {
        return "[视频]";
      } else {//开发者自定义的消息类型，需要自行处理
        return "[未知]";
      }
    }else { // 防止消息错乱
      return "";
    }
  }

  @Override public int getUnReadCount() {
    // 查询指定会话下的未读消息数
    return (int) BmobIM.getInstance().getUnReadCount(mConversation.getConversationId());
  }

  @Override public void readAllMessages() {
    mConversation.updateLocalCache();
  }

  @Override public void onClick(Context context) {
    Intent intent = new Intent();
    intent.setClass(context, ChatActivity.class);
    Bundle bundle = new Bundle();
    bundle.putSerializable(ChatActivity.CONVERSATION, mConversation);
    intent.putExtras(bundle);
    context.startActivity(intent);
  }

  @Override public void onLongClick(Context context) {
    //会话：4.5、删除会话，以下两种方式均可以删除会话
    //BmobIM.getInstance().deleteConversation(conversation.getConversationId());
    BmobIM.getInstance().deleteConversation(mConversation);
  }
}
