package com.mazaiting.imqq.model.bean;

import cn.bmob.v3.BmobUser;

/**
 * 保存用户信息
 * Created by mazaiting on 2017/9/5.
 */
public class UserInfo extends BmobUser{

  /**
   * 头像
   */
  String avatar;

  public UserInfo(){}

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }
}
