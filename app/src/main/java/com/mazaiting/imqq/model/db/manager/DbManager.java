package com.mazaiting.imqq.model.db.manager;

import android.content.Context;
import com.mazaiting.imqq.model.db.dao.DaoMaster;
import com.mazaiting.imqq.model.db.dao.DaoSession;
import com.mazaiting.imqq.model.db.dao.NewFriendDao;

/**
 * 数据库管理类
 * Created by mazaiting on 2017/9/7.
 */
abstract class DbManager {
  private static final String DB_NAME = "test.db";
  static DaoSession mDaoSession = null;

  DbManager(Context context){
    DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(context, DB_NAME);
    DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
    mDaoSession = daoMaster.newSession();
  }

}
