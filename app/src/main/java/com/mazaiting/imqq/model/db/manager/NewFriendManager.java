package com.mazaiting.imqq.model.db.manager;

import android.content.Context;
import com.mazaiting.imqq.config.Config;
import com.mazaiting.imqq.model.bean.NewFriend;
import com.mazaiting.imqq.model.db.dao.NewFriendDao;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mazaiting on 2017/9/7.
 */

public class NewFriendManager extends DbManager{
  private static NewFriendManager mManager = null;
  private final NewFriendDao mDao;

  private NewFriendManager(Context context) {
    super(context);
    mDao = mDaoSession.getNewFriendDao();
  }

  /**
   * 获取DbManager单例
   * @param context 上下文
   * @return
   */
  public static NewFriendManager getInstance(Context context){
    if (null == mManager){
      synchronized (DbManager.class){
        if (null == mManager){
          mManager = new NewFriendManager(context);
        }
      }
    }
    return mManager;
  }

  //-------------------------------------------------------------

  /**
   * 获取本地所有的添加好友信息
   * @return
   */
  public List<NewFriend> getAllNewFriend(){
    return mDao.queryBuilder().orderDesc(NewFriendDao.Properties.Time).list();
  }

  /**
   * 创建或更新朋友信息
   * @param info 新朋友
   * @return 返回插入或修改的id
   */
  public long insertOrUpdateNewFriend(NewFriend info){
    NewFriend newFriend = getNewFriend(info.getUid(), info.getTime());
    if (null == newFriend){
      return mDao.insertOrReplace(info);
    } else {
      return -1;
    }
  }

  /**
   * 获取本地的好友请求
   * @param uid uid
   * @param time 创建时间
   */
  private NewFriend getNewFriend(String uid, Long time) {
    return mDao.queryBuilder().where(NewFriendDao.Properties.Uid.eq(uid))
        .where(NewFriendDao.Properties.Time.eq(time)).build().unique();
  }

  /**
   * 是否有新的好友邀请
   * @return true 有好友请求
   *          false 无好友请求
   */
  public boolean hasNewFriendInvitation(){
    List<NewFriend> infos = getNoVerifyNewFriend();
    if (null != infos && infos.size() > 0){
      return true;
    } else {
      return false;
    }
  }

  /**
   * 获取未读的好友邀请
   * @return
   */
  public int getNewInvitationCount(){
    List<NewFriend> infos = getNoVerifyNewFriend();
    if (null != infos && infos.size() > 0){
      return infos.size();
    } else {
      return 0;
    }
  }


  /**
   * 获取所有未读未验证的好友请求
   * @return 新朋友列表
   */
  private List<NewFriend> getNoVerifyNewFriend() {
    return mDao.queryBuilder().where(NewFriendDao.Properties.Status.eq(Config.STATUS_VERIFY_NONE))
        .build().list();
  }

  /**
   * 批量更新未读未验证的状态为已读
   */
  public void updateBatchStatus(){
    List<NewFriend> infos = getNoVerifyNewFriend();
    if (null!=infos && infos.size() > 0){
      int size = infos.size();
      List<NewFriend> all = new ArrayList<>();
      for (int i=0;i<size;i++){
        NewFriend msg = infos.get(i);
        msg.setStatus(Config.STATUS_VERIFY_READED);
        all.add(msg);
      }
      insertBatchMessages(infos);
    }
  }

  /**
   * 批量插入消息
   * @param infos
   */
  private void insertBatchMessages(List<NewFriend> infos) {
    mDao.insertOrReplaceInTx(infos);
  }

  /**
   * 修改指定好友请求的状态
   * @param newFriend 新朋友
   * @param status 状态
   * @return
   */
  public long updateNewFriend(NewFriend newFriend,int status){
    newFriend.setStatus(status);
    return mDao.insertOrReplace(newFriend);
  }

  /**
   * 删除指定的添加请求
   * @param newFriend
   */
  public void deleteNewFriend(NewFriend newFriend){
    mDao.delete(newFriend);
  }
}
