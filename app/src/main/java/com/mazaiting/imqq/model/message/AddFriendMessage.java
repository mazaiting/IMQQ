package com.mazaiting.imqq.model.message;

import android.text.TextUtils;
import cn.bmob.newim.bean.BmobIMExtraMessage;
import cn.bmob.newim.bean.BmobIMMessage;
import com.mazaiting.imqq.config.Config;
import com.mazaiting.imqq.model.bean.NewFriend;
import com.orhanobut.logger.Logger;
import org.json.JSONObject;

/**
 * 添加好友请求
 * 1. 自定义消息类型，用于发送添加好友请求
 * 2. 自定义添加好友的消息类型
 * Created by mazaiting on 2017/9/6.
 */
public class AddFriendMessage extends BmobIMExtraMessage{
  public static final String ADD = "add";
  public AddFriendMessage(){}

  /**
   * 将BmobIMMessage装换为NewFriend
   * @param msg
   * @return
   */
  public static NewFriend convert(BmobIMMessage msg){
    NewFriend newFriend = new NewFriend();
    newFriend.setMsg(msg.getContent());
    newFriend.setTime(msg.getCreateTime());
    newFriend.setStatus(Config.STATUS_VERIFY_NONE);
    try {
      String extra = msg.getExtra();
      if (!TextUtils.isEmpty(extra)){
        JSONObject jsonObject = new JSONObject(extra);
        newFriend.setName(jsonObject.getString("name"));
        newFriend.setAvatar(jsonObject.getString("avatar"));
        newFriend.setUid(jsonObject.getString("uid"));
      } else {
        Logger.e("AddFriendMessage的extra为空");
      }
    }catch (Exception e){
      e.printStackTrace();
    }
    return newFriend;
  }

  @Override public String getMsgType() {
    return ADD;
  }

  @Override public boolean isTransient() {
    //设置为true,表明为暂态消息，那么这条消息并不会保存到本地db中，SDK只负责发送出去
    //设置为false,则会保存到指定会话的数据库中
    return true;
  }
}
