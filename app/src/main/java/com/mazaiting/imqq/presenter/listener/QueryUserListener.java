package com.mazaiting.imqq.presenter.listener;

import cn.bmob.newim.listener.BmobListener1;
import cn.bmob.v3.exception.BmobException;
import com.mazaiting.imqq.model.bean.UserInfo;

/**
 * 查询用户信息监听
 * Created by mazaiting on 2017/9/6.
 */
public abstract class QueryUserListener extends BmobListener1<UserInfo>{
  @Override protected void postDone(UserInfo userInfo, BmobException e) {
    done(userInfo, e);
  }

  /**
   * 执行查询后的方法
   * @param userInfo
   * @param e
   */
  public abstract void done(UserInfo userInfo, BmobException e);
}
