package com.mazaiting.imqq.presenter.listener;

import cn.bmob.newim.listener.BmobListener1;
import cn.bmob.v3.exception.BmobException;

/**
 * 更新缓存监听
 * Created by mazaiting on 2017/9/6.
 */

public abstract class UpdateCacheListener extends BmobListener1{

  @Override protected void postDone(Object o, BmobException e) {
    done(e);
  }

  /**
   * 执行异常
   * @param e
   */
  public abstract void done(BmobException e);
}
