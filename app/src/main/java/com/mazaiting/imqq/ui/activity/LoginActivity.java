package com.mazaiting.imqq.ui.activity;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.LogInListener;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.model.bean.UserInfo;
import com.mazaiting.imqq.presenter.user.UserManager;
import com.mazaiting.imqq.ui.base.BaseActivity;

public class LoginActivity extends BaseActivity {

  @BindView(R.id.et_username_login)
  EditText etUsername;
  @BindView(R.id.et_password_login)
  EditText etPassword;
  @BindView(R.id.btn_login_login)
  Button btnLogin;

  @OnClick(R.id.btn_login_login)
  public void onLogin(){
    btnLogin.setClickable(false);
    String username = etUsername.getText().toString().trim();
    String password = etPassword.getText().toString().trim();
    UserManager.getInstance().login(username, password, new LogInListener<UserInfo>() {
      @Override public void done(UserInfo userInfo, BmobException e) {
        if (e == null){
          Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
          newIntent(MainActivity.class, null);
          finish();
        } else {
          Toast.makeText(LoginActivity.this, "登录失败,请输入正确的用户名与密码！", Toast.LENGTH_SHORT).show();
        }
        btnLogin.setClickable(true);
      }
    });
  }

  @OnClick(R.id.tv_register_login)
  public void onRegister(){
    newIntent(RegisterActivity.class, null);
  }

  @Override protected void initData() {}
  @Override protected int getLayoutId() {
    return R.layout.activity_login;
  }
}
