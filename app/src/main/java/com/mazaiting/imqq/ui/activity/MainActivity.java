package com.mazaiting.imqq.ui.activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.BmobIM;
import cn.bmob.newim.notification.BmobNotificationManager;
import com.jpeng.jptabbar.JPTabBar;
import com.jpeng.jptabbar.OnTabSelectListener;
import com.jpeng.jptabbar.anno.NorIcons;
import com.jpeng.jptabbar.anno.SeleIcons;
import com.jpeng.jptabbar.anno.Titles;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.presenter.service.ConnectService;
import com.mazaiting.imqq.ui.adapter.adapter.MainViewPagerAdapter;
import com.mazaiting.imqq.ui.base.BaseActivity;
import com.mazaiting.imqq.ui.base.BaseFragment;
import com.mazaiting.imqq.utils.PopupWindowFactory;
import com.mazaiting.imqq.utils.ServiceUtil;
import com.mazaiting.imqq.utils.UIUtils;

public class MainActivity extends BaseActivity implements OnTabSelectListener {
  public static final int MESSAGE = BaseFragment.MESSAGE;
  public static final int CONTACTS = BaseFragment.CONTACTS;
  public static final int DISCOVERY = BaseFragment.DISCOVERY;
  public static final int SETTINGS = BaseFragment.SETTINGS;
  @BindView(R.id.viewpager_main)
  ViewPager mViewPager;
  @BindView(R.id.bottomTabBar_main)
  JPTabBar mBottomTabBar;
  @BindView(R.id.toolbar_include_toolbar)
  Toolbar mToolbar;
  private PopupWindow mPopupWindow;
  @Titles
  private static final String[] mTitles = {
      "消息",
      "联系人",
      "发现",
      "设置"
  };
  @SeleIcons
  private static final int[] mSeleIcons = {
      R.mipmap.message_press,
      R.mipmap.contacts_press,
      R.mipmap.discovery_press,
      R.mipmap.settings_press
  };
  @NorIcons
  private static final int[] mNormalIcons = {
      R.mipmap.message_normal,
      R.mipmap.contacts_normal,
      R.mipmap.discovery_normal,
      R.mipmap.settings_normal
  };
  /**
   * ViewPager适配器
   */
  private MainViewPagerAdapter mAdapter;

  @Override protected void initData() {
    mAdapter = new MainViewPagerAdapter(this, getSupportFragmentManager());
    mViewPager.setAdapter(mAdapter);
    mBottomTabBar.setContainer(mViewPager);
    mBottomTabBar.setTabListener(this);
    initToolBar();
  }

  /**
   * 初始化工具栏
   */
  private void initToolBar() {
    setSupportActionBar(mToolbar);
    if (null != getSupportActionBar()){
      getSupportActionBar().setTitle("QQ");
    }
    mToolbar.setTitleTextColor(UIUtils.getColor(R.color.white));
  }

  /**
   * 显示菜单
   */
  private void showMenu() {
    View menuView = View.inflate(this, R.layout.popup_menu_main, null);
    //发起群聊
    menuView.findViewById(R.id.itemCreateGroupCheat_popup_menu_main).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //startActivityFragment(new Intent(MainActivity.this, TeamCheatCreateActvitiy.class));
        //mPopupWindow.dismiss();
      }
    });
    //添加朋友
    menuView.findViewById(R.id.itemAddFriend_popup_menu_main).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //startActivityForResult(new Intent(MainActivity.this, NewFriendActivity.class), MainActivity.REQ_CLEAR_UNREAD);
        //mPopupWindow.dismiss();
      }
    });
    //扫一扫
    menuView.findViewById(R.id.itemScan_popup_menu_main).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //startActivityFragment(new Intent(MainActivity.this, ScanActivity.class));
        //mPopupWindow.dismiss();
      }
    });
    //帮助与反馈
    menuView.findViewById(R.id.itemHelpAndFeedback_popup_menu_main).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
        //intent.putExtra("url", AppConst.Url.HELP_FEEDBACK);
        //startActivityFragment(intent);
        //mPopupWindow.dismiss();
      }
    });
    mPopupWindow = PopupWindowFactory.getPopupWindowAtLocation(menuView, mViewPager, Gravity.END | Gravity.TOP, UIUtils.dip2Px(12), mToolbar.getHeight() + getStatusBarHeight());
  }

  /**
   * 连接服务器
   */
  private void connectServer() {
    if (!ServiceUtil.isServiceRunning(this, ConnectService.class.getName())){
      startService(new Intent(this, ConnectService.class));
    }
  }

  @Override protected void onMessage() {
    super.onMessage();
    Toast.makeText(this, "主界面更新", Toast.LENGTH_SHORT).show();
    mBottomTabBar.showCircleBadge(MESSAGE);
  }

  @Override protected void onContact() {
    super.onContact();
    mBottomTabBar.showCircleBadge(CONTACTS);
  }

  /**
   * 检查更新
   */
  private void checkRedPoint() {

    // 获取全部未读消息
    int count = (int) BmobIM.getInstance().getAllUnReadCount();
    Toast.makeText(this, count+"", Toast.LENGTH_SHORT).show();
    if (count > 0){
      // 有未读消息
      mBottomTabBar.showBadge(0, count);
    } else {
      // 无未读消息
    }

    // 好友管理： 是否有好友添加的请求
    // 数据库操作
  }


  @Override protected int getLayoutId() {
    return R.layout.activity_main;
  }

  @Override protected void onResume() {
    super.onResume();
    //进入应用后，通知栏应取消
    BmobNotificationManager.getInstance(this).cancelNotification();
    connectServer();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    //清理导致内存泄露的资源
    BmobIM.getInstance().clear();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    new MenuInflater(this).inflate(R.menu.menu, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case R.id.itemSearch_menu_main:
        hideSoftInputView();
        newIntent(SearchUserActivity.class, null);//添加用户
        break;
      case R.id.itemMore_menu_main:
        showMenu();
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  /**
   * 底部导航选择监听
   * @param index 当前位置
   */
  @Override public void onTabSelect(int index) {
    switch (index){
      case MESSAGE:
        //mAdapter.updateMessageFragment();
        mBottomTabBar.hideBadge(MESSAGE);
        break;
      case CONTACTS:
        //mAdapter.updateContactsFragment();
        mBottomTabBar.hideBadge(CONTACTS);
        break;
      case DISCOVERY:
        break;
      case SETTINGS:
        break;
    }
  }

  /**
   * 获取底部导航JPTabBar
   * @return
   */
  public JPTabBar getBottomTabBar() {
    return mBottomTabBar;
  }

  @Override public void onBackPressed() {
    super.onBackPressed();
  }
}
