package com.mazaiting.imqq.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import cn.bmob.newim.BmobIM;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMUserInfo;
import cn.bmob.newim.core.BmobIMClient;
import cn.bmob.newim.core.ConnectionStatus;
import cn.bmob.newim.listener.ConnectStatusChangeListener;
import cn.bmob.newim.listener.MessageSendListener;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.model.message.AddFriendMessage;
import com.mazaiting.imqq.model.bean.UserInfo;
import com.mazaiting.imqq.ui.activity.message.ChatActivity;
import com.mazaiting.imqq.ui.base.BaseActivity;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import com.orhanobut.logger.Logger;
import java.util.HashMap;
import java.util.Map;

public class OtherUserInfoActivity extends BaseActivity{
  /**陌生人*/
  public static final int STRANGER = 0x01;
  /**朋友*/
  public static final int FRIEND = 0x02;
  /**用户信息*/
  public static final String USERINFO = "userinfo";
  /**关系*/
  public static final String RELATIONSHIP = "relationship";
  /**头像*/
  @BindView(R.id.iv_avatar_other_user_info)
  ImageView ivAvatar;
  /**用户名*/
  @BindView(R.id.tv_name_other_user_info)
  TextView tvName;
  /**添加好友按钮*/
  @BindView(R.id.btn_add_friend_other_user_info)
  Button btnAdd;
  /**聊天按钮*/
  @BindView(R.id.btn_chat_other_user_info)
  Button btnChat;

  /**聊天对象*/
  private BmobIMUserInfo mIMUserInfo;
  /**用户信息*/
  private UserInfo mUserInfo;
  /**关系值*/
  private int mShip;

  @OnClick({R.id.btn_add_friend_other_user_info,R.id.btn_chat_other_user_info})
  public void onViewClicked(View view){
    switch (view.getId()){
      case R.id.btn_add_friend_other_user_info:
        sendAddFriendMessage();
        break;
      case R.id.btn_chat_other_user_info:
        chat();
        break;
    }
  }

  /**
   * 与陌生人聊天
   */
  private void chat() {
    // 创建会话信息
    BmobIMConversation conversation =
        BmobIM.getInstance().startPrivateConversation(mIMUserInfo, null);
    Bundle bundle = new Bundle();
    bundle.putSerializable(ChatActivity.CONVERSATION, conversation);
    newIntent(ChatActivity.class, bundle);
  }

  /**
   * 发送好友请求
   */
  private void sendAddFriendMessage() {
    // 创建一个暂态会话入口，发送好友请求
    BmobIMConversation conversation =
        BmobIM.getInstance().startPrivateConversation(mIMUserInfo, true, null);
    // 根据会话入口获取消息管理，发送好友请求
    BmobIMConversation messageManager = BmobIMConversation.obtain(BmobIMClient.getInstance(), conversation);
    // 获得当前用户
    UserInfo currentUser = BmobUser.getCurrentUser(UserInfo.class);
    // 新建添加朋友消息
    AddFriendMessage msg = new AddFriendMessage();
    // 给对方的一个留言信息
    msg.setContent("很高兴认识你，可以加个好友吗?");
    Map<String, Object> map = new HashMap<>();
    map.put("name", currentUser.getUsername());// 发送者姓名
    map.put("avatar",currentUser.getAvatar());//发送者头像
    map.put("uid",currentUser.getObjectId());//发送者的uid
    msg.setExtraMap(map);
    messageManager.sendMessage(msg, new MessageSendListener() {
      @Override public void done(BmobIMMessage bmobIMMessage, BmobException e) {
        if (e == null) {//发送成功
          Toast.makeText(OtherUserInfoActivity.this, "好友请求发送成功，等待验证", Toast.LENGTH_SHORT).show();
        } else {//发送失败
          Toast.makeText(OtherUserInfoActivity.this, "好友请求发送失败", Toast.LENGTH_SHORT).show();
        }
      }
    });
  }

  @Override protected void initData() {
    if (mShip == FRIEND){ // 如果关系值为朋友，则隐藏添加好友按钮
      btnAdd.setVisibility(View.GONE);
      btnChat.setText("聊天");
    }
    tvName.setText(mUserInfo.getUsername());
    //加载头像
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar, mUserInfo.getAvatar(), R.mipmap.header);
    // 创建聊天对象
    mIMUserInfo = new BmobIMUserInfo(mUserInfo.getObjectId(), mUserInfo.getUsername(), mUserInfo.getAvatar());
  }

  @Override protected void getIntentData() {
    Intent intent = getIntent();
    // 获得用户信息
    mUserInfo = (UserInfo) intent.getSerializableExtra(USERINFO);
    // 获取关系值，默认为陌生人
    mShip = intent.getIntExtra(RELATIONSHIP, STRANGER);
  }

  @Override protected int getLayoutId() {
    return R.layout.activity_other_user_info;
  }
}
