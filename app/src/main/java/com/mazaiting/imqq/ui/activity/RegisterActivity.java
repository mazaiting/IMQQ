package com.mazaiting.imqq.ui.activity;

import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.LogInListener;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.model.bean.UserInfo;
import com.mazaiting.imqq.presenter.user.UserManager;
import com.mazaiting.imqq.ui.base.BaseActivity;

public class RegisterActivity extends BaseActivity {
  @BindView(R.id.et_username_register)
  EditText etUsername;
  @BindView(R.id.et_password_register)
  EditText etPassword;
  @BindView(R.id.et_password_again_register)
  EditText etPwdAgain;

  @OnClick(R.id.btn_register_register)
  public void onRegister(){
    final String username = etUsername.getText().toString().trim();
    String password = etPassword.getText().toString().trim();
    String pwdAgain = etPwdAgain.getText().toString().trim();
    UserManager.getInstance().registerUser(username, password, pwdAgain, new LogInListener<UserInfo>() {
      @Override public void done(UserInfo userInfo, BmobException e) {
        if (e == null){
          Toast.makeText(RegisterActivity.this, "注册成功", Toast.LENGTH_SHORT).show();
          finish();
        } else {
          Toast.makeText(RegisterActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
      }
    });
  }

  @Override protected void initData() {}

  @Override protected int getLayoutId() {
    return R.layout.activity_register;
  }


}
