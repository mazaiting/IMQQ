package com.mazaiting.imqq.ui.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.model.bean.UserInfo;
import com.mazaiting.imqq.ui.adapter.adapter.SearchUserAdapter;
import com.mazaiting.imqq.ui.base.BaseActivity;
import java.util.List;

public class SearchUserActivity extends BaseActivity {
  @BindView(R.id.et_find_name_search_user)
  EditText etFindName;
  @BindView(R.id.btn_search_search_user)
  Button btnSearch;
  @BindView(R.id.sw_refresh_search_user)
  SwipeRefreshLayout swRefresh;
  @BindView(R.id.rc_view_search_user)
  RecyclerView rcView;
  private SearchUserAdapter mAdapter;

  @OnClick(R.id.btn_search_search_user)
  public void onSearchUser(){
    String username = etFindName.getText().toString().trim();
    if (TextUtils.isEmpty(username)){
      Toast.makeText(SearchUserActivity.this, "请输入用户名", Toast.LENGTH_SHORT).show();
      return;
    }
    BmobQuery<UserInfo> userQuery = new BmobQuery<>();
    userQuery.addWhereEqualTo("username", username);
    userQuery.findObjects(new FindListener<UserInfo>() {
      @Override public void done(List<UserInfo> list, BmobException e) {
        if (e == null) {
          if (list.size() > 0) {
            // 成功
            mAdapter.setData(list);
          } else {
            // 失败
            Toast.makeText(SearchUserActivity.this, "无此用户信息", Toast.LENGTH_SHORT).show();
          }
        } else {
          Toast.makeText(SearchUserActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
      }
    });
  }

  @Override protected void initData() {
    mAdapter = new SearchUserAdapter();
    rcView.setLayoutManager(new LinearLayoutManager(this));
    rcView.setAdapter(mAdapter);
  }

  @Override protected int getLayoutId() {
    return R.layout.activity_search_user;
  }
}
