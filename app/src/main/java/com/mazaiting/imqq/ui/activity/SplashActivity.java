package com.mazaiting.imqq.ui.activity;

import android.os.Handler;
import android.os.Looper;
import butterknife.OnClick;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.model.bean.UserInfo;
import com.mazaiting.imqq.presenter.user.UserManager;
import com.mazaiting.imqq.ui.base.BaseActivity;

public class SplashActivity extends BaseActivity {

  @OnClick(R.id.btn_login_splash)
  public void onBtnLogin(){
    newIntent(LoginActivity.class,null);
    finish();
  }

  @OnClick(R.id.btn_register_splash)
  public void onBtnRegister(){
    newIntent(RegisterActivity.class, null);
    finish();
  }

  @Override protected void initData() {
    Handler handler =new Handler(Looper.getMainLooper());
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        UserInfo user = UserManager.getInstance().getCurrentUser();
        if (user == null) {
          newIntent(LoginActivity.class, null);
        }else{
          newIntent(MainActivity.class, null);
        }
        finish();
      }
    },0);
  }

  @Override protected int getLayoutId() {
    return R.layout.activity_splash;
  }
}
