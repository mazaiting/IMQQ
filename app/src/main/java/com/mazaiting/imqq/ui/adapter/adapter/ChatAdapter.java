package com.mazaiting.imqq.ui.adapter.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMMessageType;
import cn.bmob.v3.BmobUser;
import com.mazaiting.imqq.ui.adapter.holder.AgreeHolder;
import com.mazaiting.imqq.ui.adapter.holder.ReceiveImageHolder;
import com.mazaiting.imqq.ui.adapter.holder.ReceiveLocationHolder;
import com.mazaiting.imqq.ui.adapter.holder.ReceiveTextHolder;
import com.mazaiting.imqq.ui.adapter.holder.ReceiveVideoHolder;
import com.mazaiting.imqq.ui.adapter.holder.ReceiveVoiceHolder;
import com.mazaiting.imqq.ui.adapter.holder.SendImageHolder;
import com.mazaiting.imqq.ui.adapter.holder.SendLocationHolder;
import com.mazaiting.imqq.ui.adapter.holder.SendTextHolder;
import com.mazaiting.imqq.ui.adapter.holder.SendVideoHolder;
import com.mazaiting.imqq.ui.adapter.holder.SendVoiceHolder;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mazaiting on 2017/9/7.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  /** 文本 */
  private final int TYPE_RECEIVER_TXT = 0x00;
  /** 文本 */
  private final int TYPE_SEND_TXT = 0x01;
  /** 图片 */
  private final int TYPE_SEND_IMAGE = 0x02;
  /** 图片 */
  private final int TYPE_RECEIVER_IMAGE = 0x03;
  /** 位置 */
  private final int TYPE_SEND_LOCATION = 0x04;
  /** 位置 */
  private final int TYPE_RECEIVER_LOCATION = 0x05;
  /** 语音 */
  private final int TYPE_SEND_VOICE = 0x06;
  /** 语音 */
  private final int TYPE_RECEIVER_VOICE = 0x07;
  /** 视频 */
  private final int TYPE_SEND_VIDEO = 0x08;
  /** 视频 */
  private final int TYPE_RECEIVER_VIDEO = 0x09;

  /** 同意添加好友成功后的样式 */
  private final int TYPE_AGREE = 0x10;

  /** 显示时间间隔:10分钟 */
  private final long TIME_INTERVAL = 10 * 60 * 1000;
  /** 聊天信息列表 */
  private List<BmobIMMessage> mMessageList = new ArrayList<>();
  /** 当前UID */
  private String mCurrentUid = "";
  /** 会话 */
  BmobIMConversation mConversation;

  public ChatAdapter(BmobIMConversation c) {
    try {
      mCurrentUid = BmobUser.getCurrentUser().getObjectId();
    } catch (Exception e) {
      e.printStackTrace();
    }
    this.mConversation = c;
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == TYPE_SEND_TXT) { // 文本
      return new SendTextHolder(parent.getContext(), parent, mConversation, onRecyclerViewListener);
    } else if (viewType == TYPE_SEND_IMAGE) { // 图片
      return new SendImageHolder(parent.getContext(), parent, mConversation,
          onRecyclerViewListener);
    } else if (viewType == TYPE_SEND_LOCATION) {
      return new SendLocationHolder(parent.getContext(), parent, mConversation,
          onRecyclerViewListener);
    } else if (viewType == TYPE_SEND_VOICE) {
      return new SendVoiceHolder(parent.getContext(), parent, mConversation,
          onRecyclerViewListener);
    } else if (viewType == TYPE_SEND_VIDEO) {
      return new SendVideoHolder(parent.getContext(), parent, mConversation,
          onRecyclerViewListener);
    } else if (viewType == TYPE_RECEIVER_TXT) {
      return new ReceiveTextHolder(parent.getContext(), parent, onRecyclerViewListener);
    } else if (viewType == TYPE_RECEIVER_IMAGE) {
      return new ReceiveImageHolder(parent.getContext(), parent, onRecyclerViewListener);
    } else if (viewType == TYPE_RECEIVER_LOCATION) {
      return new ReceiveLocationHolder(parent.getContext(), parent, onRecyclerViewListener);
    } else if (viewType == TYPE_RECEIVER_VOICE) {
      return new ReceiveVoiceHolder(parent.getContext(), parent, onRecyclerViewListener);
    } else if (viewType == TYPE_RECEIVER_VIDEO) {
      return new ReceiveVideoHolder(parent.getContext(), parent, onRecyclerViewListener);
    } else if (viewType == TYPE_AGREE) {
      return new AgreeHolder(parent.getContext(), parent, onRecyclerViewListener);
    } else {//开发者自定义的其他类型，可自行处理
      return null;
    }
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    ((BaseViewHolder) holder).bindData(mMessageList.get(position));
    if (holder instanceof ReceiveTextHolder) {
      ((ReceiveTextHolder)holder).showTime(shouldShowTime(position));
    } else if (holder instanceof ReceiveImageHolder) {
      ((ReceiveImageHolder)holder).showTime(shouldShowTime(position));
    }else if (holder instanceof ReceiveLocationHolder) {
      ((ReceiveLocationHolder)holder).showTime(shouldShowTime(position));
    }else if (holder instanceof ReceiveVoiceHolder) {
      ((ReceiveVoiceHolder)holder).showTime(shouldShowTime(position));
    }else if (holder instanceof SendTextHolder) {
      ((SendTextHolder)holder).showTime(shouldShowTime(position));
    }else if (holder instanceof SendImageHolder) {
      ((SendImageHolder)holder).showTime(shouldShowTime(position));
    }else if (holder instanceof SendLocationHolder) {
      ((SendLocationHolder)holder).showTime(shouldShowTime(position));
    }else if (holder instanceof SendVoiceHolder) {
      ((SendVoiceHolder)holder).showTime(shouldShowTime(position));
    }else if (holder instanceof SendVideoHolder) {//随便模拟的视频类型
      ((SendVideoHolder)holder).showTime(shouldShowTime(position));
    }else if (holder instanceof ReceiveVideoHolder) {
      ((ReceiveVideoHolder)holder).showTime(shouldShowTime(position));
    }else if (holder instanceof AgreeHolder) {//同意添加好友成功后的消息
      ((AgreeHolder)holder).showTime(shouldShowTime(position));
    }
  }

  @Override public int getItemViewType(int position) {
    BmobIMMessage message = mMessageList.get(position);
    if (message.getMsgType().equals(BmobIMMessageType.IMAGE.getType())) {
      return message.getFromId().equals(mCurrentUid) ? TYPE_SEND_IMAGE : TYPE_RECEIVER_IMAGE;
    } else if (message.getMsgType().equals(BmobIMMessageType.LOCATION.getType())) {
      return message.getFromId().equals(mCurrentUid) ? TYPE_SEND_LOCATION : TYPE_RECEIVER_LOCATION;
    } else if (message.getMsgType().equals(BmobIMMessageType.VOICE.getType())) {
      return message.getFromId().equals(mCurrentUid) ? TYPE_SEND_VOICE : TYPE_RECEIVER_VOICE;
    } else if (message.getMsgType().equals(BmobIMMessageType.TEXT.getType())) {
      return message.getFromId().equals(mCurrentUid) ? TYPE_SEND_TXT : TYPE_RECEIVER_TXT;
    } else if (message.getMsgType().equals(BmobIMMessageType.VIDEO.getType())) {
      return message.getFromId().equals(mCurrentUid) ? TYPE_SEND_VIDEO : TYPE_RECEIVER_VIDEO;
    } else if (message.getMsgType().equals("agree")) {//显示欢迎
      return TYPE_AGREE;
    } else {
      return -1;
    }
  }

  @Override public int getItemCount() {
    return mMessageList.size();
  }

  /**
   * 查找位置
   *
   * @param message 消息
   */
  public int findPosition(BmobIMMessage message) {
    int index = this.getCount();
    int position = -1;
    while (index-- > 0) {
      if (message.equals(this.getItem(index))) {
        position = index;
        break;
      }
    }
    return position;
  }

  /**
   * 根据id查找位置
   */
  public int findPosition(long id) {
    int index = this.getCount();
    int position = -1;
    while (index-- > 0) {
      if (this.getItemId(position) == id) {
        position = index;
        break;
      }
    }
    return position;
  }

  /**
   * 获取当前消息条数
   */
  public int getCount() {
    return this.mMessageList == null ? 0 : this.mMessageList.size();
  }

  /**
   * 获取第一条消息
   */
  public BmobIMMessage getFirstMessage() {
    if (null != mMessageList && mMessageList.size() > 0) {
      return mMessageList.get(0);
    } else {
      return null;
    }
  }

  /**
   * 移除消息
   */
  public void remove(int position) {
    mMessageList.remove(position);
    notifyDataSetChanged();
  }

  /**
   * 获取消息
   */
  public BmobIMMessage getItem(int position) {
    return this.mMessageList == null ? null
        : (position >= this.mMessageList.size()) ? null : this.mMessageList.get(position);
  }

  /**
   * 添加单条消息
   */
  public void addMessage(BmobIMMessage message) {
    mMessageList.addAll(Arrays.asList(message));
    notifyDataSetChanged();
  }

  /**
   * 添加多条消息
   */
  public void addMessages(List<BmobIMMessage> messages) {
    mMessageList.addAll(0, messages);
    notifyDataSetChanged();
  }

  private OnRecyclerViewListener onRecyclerViewListener;

  public void setOnRecyclerViewListener(OnRecyclerViewListener onRecyclerViewListener) {
    this.onRecyclerViewListener = onRecyclerViewListener;
  }

  /**
   * 显示时间
   */
  private boolean shouldShowTime(int position) {
    if (position == 0) {
      return true;
    }
    long lastTime = mMessageList.get(position - 1).getCreateTime();
    long curTime = mMessageList.get(position).getCreateTime();
    return curTime - lastTime > TIME_INTERVAL;
  }
}
