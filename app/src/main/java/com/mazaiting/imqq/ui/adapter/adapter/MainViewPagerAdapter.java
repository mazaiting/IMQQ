package com.mazaiting.imqq.ui.adapter.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;
import android.widget.Toast;
import com.mazaiting.imqq.ui.base.BaseFragment;
import com.mazaiting.imqq.ui.fragment.ContactsFragment;
import com.mazaiting.imqq.ui.fragment.DiscoveryFragment;
import com.mazaiting.imqq.ui.fragment.MessageFragment;
import com.mazaiting.imqq.ui.fragment.SettingsFragment;
import com.orhanobut.logger.Logger;
import java.util.ArrayList;
import java.util.List;

/**
 * 主页ViewPager适配器
 * Created by mazaiting on 2017/9/7.
 */
public class MainViewPagerAdapter extends FragmentPagerAdapter {
  public static final int MESSAGE = BaseFragment.MESSAGE;
  public static final int CONTACTS = BaseFragment.CONTACTS;
  public static final int DISCOVERY = BaseFragment.DISCOVERY;
  public static final int SETTINGS = BaseFragment.SETTINGS;
  private Context mContext;
  /**
   * Fragment集合
   */
  private static List<Fragment> mFragmentList;

  static {
    // 初始化Fragment集合
    mFragmentList = new ArrayList<>();
    mFragmentList.add(new MessageFragment());
    mFragmentList.add(new ContactsFragment());
    mFragmentList.add(new DiscoveryFragment());
    mFragmentList.add(new SettingsFragment());
  }
  public MainViewPagerAdapter(Context context, FragmentManager fm) {
    super(fm);
  }

  @Override public Fragment getItem(int position) {
    return mFragmentList.get(position);
  }

  @Override public int getCount() {
    return mFragmentList.size();
  }
}
