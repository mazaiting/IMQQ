package com.mazaiting.imqq.ui.adapter.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.mazaiting.imqq.model.bean.UserInfo;
import com.mazaiting.imqq.ui.adapter.holder.SearchUserHolder;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mazaiting on 2017/9/6.
 */

public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserHolder>{
  private List<UserInfo> users = new ArrayList<>();
  private OnRecyclerViewListener onRecyclerViewListener;

  @Override public SearchUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new SearchUserHolder(parent.getContext(), parent, onRecyclerViewListener);
  }

  @Override public void onBindViewHolder(SearchUserHolder holder, int position) {
    holder.bindData(users.get(position));
  }

  /**获取用户
   * @param position
   * @return
   */
  public UserInfo getItem(int position){
    return users.get(position);
  }

  @Override public int getItemCount() {
    return users.size();
  }

  @Override
  public int getItemViewType(int position) {
    return 1;
  }

  /**
   * 添加数据
   * @param list
   */
  public void setData(List<UserInfo> list) {
    users.clear();
    if (null != list) {
      users.addAll(list);
      notifyDataSetChanged();
    }
  }

  public void setOnRecyclerViewListener(OnRecyclerViewListener onRecyclerViewListener) {
    this.onRecyclerViewListener = onRecyclerViewListener;
  }
}
