package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMMessage;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.TimeUtil;

/**
 * 同意添加好友的agree类型
 * Created by mazaiting on 2017/9/11.
 */
public class AgreeHolder extends BaseViewHolder<BmobIMMessage>{
  @BindView(R.id.tv_time_agree)
  protected TextView tvTime;

  @BindView(R.id.tv_message_agree)
  protected TextView tvMessage;

  public AgreeHolder(Context context, ViewGroup root,OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_agree, listener);
  }

  @Override public void bindData(BmobIMMessage message) {
    String time = TimeUtil.getTime(true, message.getCreateTime());
    String content = message.getContent();
    tvMessage.setText(content);
    tvMessage.setText(time);
  }

  /**
   * 是否显示时间
   * @param isShow 是否显示
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
