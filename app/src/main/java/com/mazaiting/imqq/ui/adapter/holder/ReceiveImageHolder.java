package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMImageMessage;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMUserInfo;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import com.mazaiting.imqq.utils.TimeUtil;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * 接收到图片
 * Created by mazaiting on 2017/9/11.
 */

public class ReceiveImageHolder extends BaseViewHolder<BmobIMMessage> {
  @BindView(R.id.tv_time_received_image)
  protected TextView tvTime;
  @BindView(R.id.iv_avatar_received_image)
  protected ImageView ivAvatar;
  @BindView(R.id.iv_picture_received_image)
  protected ImageView ivPicture;
  @BindView(R.id.progress_load_received_image)
  protected ProgressBar mProgressBar;

  public ReceiveImageHolder(Context context, ViewGroup root, OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_received_image, listener);
  }

  @Override public void bindData(BmobIMMessage msg) {
    //用户信息的获取必须在buildFromDB之前，否则会报错'Entity is detached from DAO context'
    final BmobIMUserInfo info = msg.getBmobIMUserInfo();
    // 加载头像
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar, info!=null ? info.getAvatar():null, R.mipmap.header);
    // 设置时间
    String time = TimeUtil.getTime(true, msg.getCreateTime());
    tvTime.setText(time);
    // 可使用buildFromDB方法转化为指定类型的消息
    final BmobIMImageMessage message = BmobIMImageMessage.buildFromDB(false, msg);
    // 显示图片
    ImageLoaderFactory.getLoader().load(ivPicture, message.getRemoteUrl(), R.mipmap.ic_launcher, new ImageLoadingListener() {
      @Override public void onLoadingStarted(String imageUri, View view) {
        mProgressBar.setVisibility(View.VISIBLE);
      }

      @Override public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
        mProgressBar.setVisibility(View.INVISIBLE);
      }

      @Override public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        mProgressBar.setVisibility(View.INVISIBLE);
      }

      @Override public void onLoadingCancelled(String imageUri, View view) {
        mProgressBar.setVisibility(View.INVISIBLE);
      }
    });

    // 头像点击
    ivAvatar.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        Toast.makeText(mContext, "点击" + info.getName() + "的头像", Toast.LENGTH_SHORT).show();
      }
    });

    // 图片点击
    ivPicture.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        Toast.makeText(mContext, "点击图片:"+message.getRemoteUrl()+"", Toast.LENGTH_SHORT).show();
        if(onRecyclerViewListener!=null){
          onRecyclerViewListener.onItemClick(getAdapterPosition());
        }
      }
    });

    ivPicture.setOnLongClickListener(new View.OnLongClickListener() {
      @Override public boolean onLongClick(View v) {
        if (onRecyclerViewListener != null) {
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });
  }

  /**
   * 显示时间
   * @param isShow 是否显示
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
