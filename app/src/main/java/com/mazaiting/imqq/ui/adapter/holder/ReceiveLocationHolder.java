package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMLocationMessage;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMUserInfo;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import com.mazaiting.imqq.utils.TimeUtil;

/**
 * 接受到位置信息
 * Created by mazaiting on 2017/9/11.
 */

public class ReceiveLocationHolder extends BaseViewHolder<BmobIMMessage>{
  @BindView(R.id.tv_time_received_location)
  protected TextView tvTime;
  @BindView(R.id.iv_avatar_received_location)
  protected ImageView ivAvatar;
  @BindView(R.id.tv_location_received_location)
  protected TextView tvLocation;

  public ReceiveLocationHolder(Context context, ViewGroup root, OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_received_location, listener);
  }

  @Override public void bindData(BmobIMMessage msg) {
    //用户信息的获取必须在buildFromDB之前，否则会报错'Entity is detached from DAO context'
    final BmobIMUserInfo info = msg.getBmobIMUserInfo();
    //加载头像
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar,info != null ? info.getAvatar() : null, R.mipmap.header);
    String time = TimeUtil.getTime(true, msg.getCreateTime());
    tvTime.setText(time);
    // 设置位置
    final BmobIMLocationMessage message = BmobIMLocationMessage.buildFromDB(msg);
    tvLocation.setText(message.getAddress());
    // 位置点击
    tvLocation.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "经度：" + message.getLongitude() + ",维度：" + message.getLatitude(), Toast.LENGTH_SHORT).show();
        if(onRecyclerViewListener!=null){
          onRecyclerViewListener.onItemClick(getAdapterPosition());
        }
      }
    });

    // 位置长点击
    tvLocation.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (onRecyclerViewListener != null) {
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });

    /**
     * 头像点击
     */
    ivAvatar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击"+info.getName()+"头像", Toast.LENGTH_SHORT).show();
      }
    });
  }

  /**
   * 显示时间
   * @param isShow 是否显示
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
