package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMUserInfo;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import java.text.SimpleDateFormat;

/**
 * 接收文本
 * Created by mazaiting on 2017/9/7.
 */

public class ReceiveTextHolder extends BaseViewHolder{
  @BindView(R.id.iv_avatar_received_message)
  ImageView ivAvatar;
  @BindView(R.id.tv_time_received_message)
  TextView tvTime;
  @BindView(R.id.tv_message_received_message)
  TextView tvMessage;
  private Context mContext;
  public ReceiveTextHolder(Context context, ViewGroup root,OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_received_message, listener);
    this.mContext = context;
  }

  @Override public void bindData(Object o) {
    final BmobIMMessage message = (BmobIMMessage) o;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
    String time = dateFormat.format(message.getCreateTime());
    tvTime.setText(time);
    final BmobIMUserInfo info = message.getBmobIMUserInfo();
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar, info != null ? info.getAvatar() : null, R.mipmap.header);
    String content = message.getContent();
    tvMessage.setText(content);
    // 头像点击
    ivAvatar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击" + info.getName() + "的头像", Toast.LENGTH_SHORT).show();
      }
    });
    // 消息点击
    tvMessage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击"+message.getContent(), Toast.LENGTH_SHORT).show();
        if(onRecyclerViewListener!=null){
          onRecyclerViewListener.onItemClick(getAdapterPosition());
        }
      }
    });

    // 消息长按
    tvMessage.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (onRecyclerViewListener != null) {
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });
  }

  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
