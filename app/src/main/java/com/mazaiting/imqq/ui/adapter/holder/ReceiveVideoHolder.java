package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMUserInfo;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import com.mazaiting.imqq.utils.TimeUtil;

/**
 * 接受到视频
 * Created by mazaiting on 2017/9/11.
 */

public class ReceiveVideoHolder extends BaseViewHolder<BmobIMMessage> {

  @BindView(R.id.iv_avatar_sent_video)
  protected ImageView ivAvatar;

  @BindView(R.id.tv_time_sent_video)
  protected TextView tvTime;

  @BindView(R.id.tv_message_sent_video)
  protected TextView tvMessage;

  public ReceiveVideoHolder(Context context, ViewGroup root, OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_received_video, listener);
  }

  @Override public void bindData(final BmobIMMessage message) {
    // 设置时间
    String time = TimeUtil.getTime(true, message.getCreateTime());
    tvTime.setText(time);
    final BmobIMUserInfo info = message.getBmobIMUserInfo();
    // 加载头像
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar,info != null ? info.getAvatar() : null, R.mipmap.header);
    String content =  message.getContent();
    tvMessage.setText("接收到的视频文件："+content);
    ivAvatar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击" + info.getName() + "的头像", Toast.LENGTH_SHORT).show();
      }
    });

    tvMessage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击"+message.getContent(), Toast.LENGTH_SHORT).show();
        if(onRecyclerViewListener!=null){
          onRecyclerViewListener.onItemClick(getAdapterPosition());
        }
      }
    });

    tvMessage.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (onRecyclerViewListener != null) {
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });
  }

  /**
   * 显示时间
   * @param isShow 是否显示
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
