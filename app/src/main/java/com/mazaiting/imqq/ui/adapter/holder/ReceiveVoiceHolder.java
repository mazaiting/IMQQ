package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMAudioMessage;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMUserInfo;
import cn.bmob.newim.core.BmobDownloadManager;
import cn.bmob.newim.listener.FileDownloadListener;
import cn.bmob.v3.exception.BmobException;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.presenter.user.UserManager;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import com.mazaiting.imqq.utils.TimeUtil;

/**
 * 接收到声音消息
 * Created by mazaiting on 2017/9/11.
 */
public class ReceiveVoiceHolder extends BaseViewHolder<BmobIMMessage> {

  @BindView(R.id.tv_time_received_voice)
  protected TextView tvTime;
  @BindView(R.id.iv_avatar_received_voice)
  protected ImageView ivAvatar;
  @BindView(R.id.iv_voice_received_voice)
  protected ImageView ivVoice;
  @BindView(R.id.tv_voice_length_received_voice)
  protected TextView tvVoiceLength;
  @BindView(R.id.progress_load_received_voice)
  protected ProgressBar mProgressBar;
  private String currentUid = "";

  public ReceiveVoiceHolder(Context context, ViewGroup root,OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_received_voice, listener);
    try{
      currentUid = UserManager.getInstance().getCurrentUser().getObjectId();
    } catch (Exception e){
      e.printStackTrace();
    }
  }

  @Override public void bindData(BmobIMMessage msg) {
    //用户信息的获取必须在buildFromDB之前，否则会报错'Entity is detached from DAO context'
    final BmobIMUserInfo info = msg.getBmobIMUserInfo();
    // 加载头像
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar,info != null ? info.getAvatar() : null, R.mipmap.header);
    // 设置时间
    String time = TimeUtil.getTime(true, msg.getCreateTime());
    tvTime.setText(time);
    // 头像点击
    ivAvatar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击" + info.getName() + "的头像", Toast.LENGTH_SHORT).show();
      }
    });
    //显示特有属性
    final BmobIMAudioMessage message = BmobIMAudioMessage.buildFromDB(false, msg);
    boolean isExists = BmobDownloadManager.isAudioExist(currentUid, message);
    if(!isExists){//若指定格式的录音文件不存在，则需要下载，因为其文件比较小，故放在此下载
      BmobDownloadManager downloadTask = new BmobDownloadManager(getContext(),msg,new FileDownloadListener() {

        @Override
        public void onStart() {
          mProgressBar.setVisibility(View.VISIBLE);
          tvVoiceLength.setVisibility(View.GONE);
          ivVoice.setVisibility(View.INVISIBLE);//只有下载完成才显示播放的按钮
        }

        @Override
        public void done(BmobException e) {
          if(e==null){
            mProgressBar.setVisibility(View.GONE);
            tvVoiceLength.setVisibility(View.VISIBLE);
            tvVoiceLength.setText(message.getDuration()+"\''");
            ivVoice.setVisibility(View.VISIBLE);
          }else{
            mProgressBar.setVisibility(View.GONE);
            tvVoiceLength.setVisibility(View.GONE);
            ivVoice.setVisibility(View.INVISIBLE);
          }
        }
      });
      downloadTask.execute(message.getContent());
    }else{
      tvVoiceLength.setVisibility(View.VISIBLE);
      tvVoiceLength.setText(message.getDuration() + "\''");
    }

    ivVoice.setOnClickListener(new NewRecordPlayClickListener(getContext(), message, ivVoice));

    ivVoice.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (onRecyclerViewListener != null) {
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });

  }

  /**
   * 显示时间
   * @param isShow 是否显示时间
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
