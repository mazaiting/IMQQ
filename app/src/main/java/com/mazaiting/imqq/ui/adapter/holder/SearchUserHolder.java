package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.model.bean.UserInfo;
import com.mazaiting.imqq.ui.activity.OtherUserInfoActivity;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;

/**
 * 查询用户--数据
 * Created by mazaiting on 2017/9/6.
 */

public class SearchUserHolder extends BaseViewHolder {
  @BindView(R.id.avatar_item_search_user)
  ImageView ivAvatar;
  @BindView(R.id.name_item_search_user)
  TextView tvName;
  @BindView(R.id.btn_add_item_search_user)
  Button btnAdd;
  private Context mContext;

  public SearchUserHolder(Context context, ViewGroup root, OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_search_user, listener);
    this.mContext = context;
  }

  @Override public void bindData(Object o) {
    final UserInfo userInfo = (UserInfo) o;
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar, userInfo.getAvatar(), R.mipmap.header);
    tvName.setText(userInfo.getUsername());
    btnAdd.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) { // 查看个人详情
        Intent intent = new Intent(mContext, OtherUserInfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(OtherUserInfoActivity.USERINFO, userInfo);
        bundle.putInt(OtherUserInfoActivity.RELATIONSHIP, OtherUserInfoActivity.STRANGER);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
      }
    });
  }
}
