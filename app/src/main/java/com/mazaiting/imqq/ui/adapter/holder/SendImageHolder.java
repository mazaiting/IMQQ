package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMImageMessage;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMSendStatus;
import cn.bmob.newim.bean.BmobIMUserInfo;
import cn.bmob.newim.listener.MessageSendListener;
import cn.bmob.v3.exception.BmobException;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import com.mazaiting.imqq.utils.TimeUtil;

/**
 * 发送图片
 * Created by mazaiting on 2017/9/11.
 */
public class SendImageHolder extends BaseViewHolder<BmobIMMessage> {
  @BindView(R.id.iv_avatar_sent_image)
  protected ImageView ivAvatar;
  @BindView(R.id.iv_fail_resend_sent_image)
  protected ImageView ivFail;
  @BindView(R.id.tv_time_sent_image)
  protected TextView tvTime;
  @BindView(R.id.iv_picture_sent_image)
  protected ImageView ivPicture;
  @BindView(R.id.tv_send_status_sent_image)
  protected TextView tvSendStatus;
  @BindView(R.id.progress_load_sent_image)
  protected ProgressBar mProgressBar;
  /**会话*/
  BmobIMConversation mConversation;

  public SendImageHolder(Context context, ViewGroup root, BmobIMConversation conversation,
      OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_sent_image, listener);
    this.mConversation = conversation;
  }

  @Override public void bindData(BmobIMMessage msg) {
    //用户信息的获取必须在buildFromDB之前，否则会报错'Entity is detached from DAO context'
    final BmobIMUserInfo info = msg.getBmobIMUserInfo();
    // 加载头像
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar, info.getAvatar(), R.mipmap.header);
    // 设置时间
    String time = TimeUtil.getTime(true, msg.getCreateTime());
    tvTime.setText(time);

    final BmobIMImageMessage message = BmobIMImageMessage.buildFromDB(true, msg);
    int status = message.getSendStatus();
    if (BmobIMSendStatus.SEND_FAILED.getStatus() == status ||
        BmobIMSendStatus.UPLOAD_FAILED.getStatus() == status){
      ivFail.setVisibility(View.VISIBLE);
      mProgressBar.setVisibility(View.GONE);
      tvSendStatus.setVisibility(View.INVISIBLE);
    } else if (BmobIMSendStatus.SENDING.getStatus() == status){
      mProgressBar.setVisibility(View.VISIBLE);
      ivFail.setVisibility(View.GONE);
      tvSendStatus.setVisibility(View.INVISIBLE);
    } else {
      tvSendStatus.setVisibility(View.INVISIBLE);
      tvSendStatus.setText("已发送");
      ivFail.setVisibility(View.GONE);
      mProgressBar.setVisibility(View.GONE);
    }

    // 加载图片
    String url = TextUtils.isEmpty(message.getRemoteUrl()) ? message.getLocalPath() : message.getRemoteUrl();
    ImageLoaderFactory.getLoader().load(ivPicture, url, R.mipmap.ic_launcher, null);

    // 设置点击
    // 头像点击
    ivAvatar.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        Toast.makeText(mContext, "点击" + info.getName() + "的头像", Toast.LENGTH_SHORT).show();
      }
    });

    // 图片点击
    ivPicture.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        Toast.makeText(mContext, "点击图片:"+(TextUtils.isEmpty(message.getRemoteUrl()) ? message.getLocalPath():message.getRemoteUrl())+"", Toast.LENGTH_SHORT).show();
        if (onRecyclerViewListener != null){
          onRecyclerViewListener.onItemClick(getAdapterPosition());
        }
      }
    });

    // 图片长点击
    ivPicture.setOnLongClickListener(new View.OnLongClickListener() {
      @Override public boolean onLongClick(View v) {
        if (onRecyclerViewListener != null){
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });

    // 失败按钮点击
    ivFail.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mConversation.resendMessage(message, new MessageSendListener() {
          @Override public void onStart(BmobIMMessage bmobIMMessage) {
            mProgressBar.setVisibility(View.VISIBLE);
            ivFail.setVisibility(View.GONE);
            tvSendStatus.setVisibility(View.INVISIBLE);
          }

          @Override public void done(BmobIMMessage bmobIMMessage, BmobException e) {
            if (e == null) {
              tvSendStatus.setVisibility(View.VISIBLE);
              tvSendStatus.setText("已发送");
              ivFail.setVisibility(View.GONE);
              mProgressBar.setVisibility(View.GONE);
            } else {
              ivFail.setVisibility(View.VISIBLE);
              mProgressBar.setVisibility(View.GONE);
              tvSendStatus.setVisibility(View.INVISIBLE);
            }
          }
        });
      }
    });


  }

  /**
   * 显示时间
   * @param isShow 是否显示
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }

}
