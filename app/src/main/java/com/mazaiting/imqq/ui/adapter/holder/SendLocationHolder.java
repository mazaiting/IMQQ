package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMImageMessage;
import cn.bmob.newim.bean.BmobIMLocationMessage;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMSendStatus;
import cn.bmob.newim.bean.BmobIMUserInfo;
import cn.bmob.newim.listener.MessageSendListener;
import cn.bmob.v3.exception.BmobException;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import com.mazaiting.imqq.utils.TimeUtil;

/**
 * 接受到位置信息
 * Created by mazaiting on 2017/9/11.
 */
public class SendLocationHolder extends BaseViewHolder<BmobIMImageMessage>{
  @BindView(R.id.tv_time_sent_location)
  protected TextView tvTime;
  @BindView(R.id.iv_avatar_sent_location)
  protected ImageView ivAvatar;
  @BindView(R.id.layout_location_sent_location)
  protected LinearLayout layoutLocation;
  @BindView(R.id.tv_location_sent_location)
  protected TextView tvLocation;
  @BindView(R.id.iv_fail_resend_sent_location)
  protected ImageView ivFail;
  @BindView(R.id.tv_send_status_sent_location)
  protected TextView tvSend;
  @BindView(R.id.progress_load_sent_location)
  protected ProgressBar mProgressBar;
  BmobIMConversation mConversation;

  public SendLocationHolder(Context context, ViewGroup root, BmobIMConversation conversation,
      OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_sent_location, listener);
    this.mConversation = conversation;
  }

  @Override public void bindData(BmobIMImageMessage msg) {
    //用户信息的获取必须在buildFromDB之前，否则会报错'Entity is detached from DAO context'
    final BmobIMUserInfo info = msg.getBmobIMUserInfo();
    // 加载头像
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar,info != null ? info.getAvatar() : null, R.mipmap.header);
    // 设置时间
    String time = TimeUtil.getTime(true, msg.getCreateTime());
    tvTime.setText(time);
    // 设置位置
    final BmobIMLocationMessage message = BmobIMLocationMessage.buildFromDB(msg);
    tvTime.setText(message.getAddress());
    int status =message.getSendStatus();
    if (status == BmobIMSendStatus.SEND_FAILED.getStatus()) {
      ivFail.setVisibility(View.VISIBLE);
      mProgressBar.setVisibility(View.GONE);
    } else if (status== BmobIMSendStatus.SENDING.getStatus()) {
      ivFail.setVisibility(View.GONE);
      mProgressBar.setVisibility(View.VISIBLE);
    } else {
      ivFail.setVisibility(View.GONE);
      mProgressBar.setVisibility(View.GONE);
    }
    // 失败点击
    ivFail.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击" + info.getName() + "的头像", Toast.LENGTH_SHORT).show();
      }
    });

    // 位置点击
    tvLocation.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "经度：" + message.getLongitude() + ",维度：" + message.getLatitude(), Toast.LENGTH_SHORT).show();
        if(onRecyclerViewListener!=null){
          onRecyclerViewListener.onItemClick(getAdapterPosition());
        }
      }
    });

    // 位置长点击
    tvLocation.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (onRecyclerViewListener != null) {
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });

    //重发
    ivFail.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mConversation.resendMessage(message, new MessageSendListener() {
          @Override
          public void onStart(BmobIMMessage msg) {
            mProgressBar.setVisibility(View.VISIBLE);
            ivFail.setVisibility(View.GONE);
            tvSend.setVisibility(View.INVISIBLE);
          }

          @Override
          public void done(BmobIMMessage msg, BmobException e) {
            if (e == null) {
              tvSend.setVisibility(View.VISIBLE);
              tvSend.setText("已发送");
              ivFail.setVisibility(View.GONE);
              mProgressBar.setVisibility(View.GONE);
            } else {
              ivFail.setVisibility(View.VISIBLE);
              mProgressBar.setVisibility(View.GONE);
              tvSend.setVisibility(View.INVISIBLE);
            }
          }
        });
      }
    });
  }

  /**
   * 是否显示时间
   * @param isShow 是否显示时间
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
