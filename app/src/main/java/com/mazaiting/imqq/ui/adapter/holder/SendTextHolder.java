package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMSendStatus;
import cn.bmob.newim.bean.BmobIMUserInfo;
import cn.bmob.newim.listener.MessageSendListener;
import cn.bmob.v3.exception.BmobException;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import java.text.SimpleDateFormat;

/**
 * 发送的文本类型
 * Created by mazaiting on 2017/9/7.
 */

public class SendTextHolder extends BaseViewHolder {
  @BindView(R.id.iv_avatar_sent_message)
  ImageView ivAvatar;
  @BindView(R.id.iv_fail_resend_sent_message)
  ImageView ivFailResend;
  @BindView(R.id.tv_time_sent_message)
  TextView tvTime;
  @BindView(R.id.tv_message_sent_message)
  TextView tvMessage;
  @BindView(R.id.tv_send_status_sent_message)
  TextView tvSendStatus;
  @BindView(R.id.progress_load_sent_message)
  ProgressBar pbLoad;

  BmobIMConversation mConversation;
  private Context mContext;
  public SendTextHolder(Context context, ViewGroup root, BmobIMConversation c,
      OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_sent_message, listener);
    this.mContext = context;
    this.mConversation = c;
  }

  @Override public void bindData(Object o) {
    final BmobIMMessage message = (BmobIMMessage) o;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    final BmobIMUserInfo info = message.getBmobIMUserInfo();
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar, info != null? info.getAvatar():null, R.mipmap.header);
    String time = dateFormat.format(message.getCreateTime());
    String content = message.getContent();
    tvMessage.setText(content);
    tvTime.setText(time);

    int sendStatus = message.getSendStatus();
    if (sendStatus == BmobIMSendStatus.SEND_FAILED.getStatus()){
      ivFailResend.setVisibility(View.VISIBLE);
      pbLoad.setVisibility(View.GONE);
    } else if (sendStatus == BmobIMSendStatus.SENDING.getStatus()){
      ivFailResend.setVisibility(View.GONE);
      pbLoad.setVisibility(View.VISIBLE);
    } else {
      ivFailResend.setVisibility(View.GONE);
      pbLoad.setVisibility(View.GONE);
    }

    // 消息点击
    tvMessage.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        Toast.makeText(mContext, "点击" + message.getContent(), Toast.LENGTH_SHORT).show();
        if (onRecyclerViewListener != null){
          onRecyclerViewListener.onItemClick(getAdapterPosition());
        }
      }
    });

    // 消息长点击
    tvMessage.setOnLongClickListener(new View.OnLongClickListener() {
      @Override public boolean onLongClick(View v) {
        if (onRecyclerViewListener!=null){
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });

    // 头像点击
    ivAvatar.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        Toast.makeText(mContext, "点击" + info.getName() + "的头像", Toast.LENGTH_SHORT).show();
      }
    });

    // 重发消息
    ivFailResend.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mConversation.resendMessage(message, new MessageSendListener() {
          @Override public void onStart(BmobIMMessage bmobIMMessage) {
            pbLoad.setVisibility(View.VISIBLE);
            ivFailResend.setVisibility(View.GONE);
            tvSendStatus.setVisibility(View.INVISIBLE);
          }

          @Override public void done(BmobIMMessage bmobIMMessage, BmobException e) {
            if (e == null){
              tvSendStatus.setVisibility(View.VISIBLE);
              tvSendStatus.setText("已发送");
              ivFailResend.setVisibility(View.GONE);
              pbLoad.setVisibility(View.GONE);
            }else{
              ivFailResend.setVisibility(View.VISIBLE);
              pbLoad.setVisibility(View.GONE);
              tvSendStatus.setVisibility(View.INVISIBLE);
            }
          }
        });
      }
    });
  }

  /**
   * 显示时间
   * @param isShow
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
