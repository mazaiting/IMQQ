package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMSendStatus;
import cn.bmob.newim.bean.BmobIMUserInfo;
import cn.bmob.newim.listener.MessageSendListener;
import cn.bmob.v3.exception.BmobException;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import com.mazaiting.imqq.utils.TimeUtil;

/**
 * 发送视频类型
 * Created by mazaiting on 2017/9/11.
 */

public class SendVideoHolder extends BaseViewHolder<BmobIMMessage> {
  @BindView(R.id.iv_avatar_sent_message)
  protected ImageView ivAvatar;

  @BindView(R.id.iv_fail_resend_sent_video)
  protected ImageView ivFailResend;

  @BindView(R.id.tv_time_sent_video)
  protected TextView tvTime;

  @BindView(R.id.tv_message_sent_video)
  protected TextView tvMessage;
  @BindView(R.id.tv_send_status_sent_video)
  protected TextView tvSendStatus;

  @BindView(R.id.progress_load_sent_video)
  protected ProgressBar mProgressBar;

  BmobIMConversation mConversation;
  public SendVideoHolder(Context context, ViewGroup root, BmobIMConversation conversation,
      OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_sent_video, listener);
    this.mConversation = conversation;
  }

  @Override public void bindData(final BmobIMMessage msg) {
    final BmobIMUserInfo info = msg.getBmobIMUserInfo();
    // 加载头像
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar,info != null ? info.getAvatar() : null, R.mipmap.header);

    String time = TimeUtil.getTime(true, msg.getCreateTime());
    String content = msg.getContent();
    tvMessage.setText("发送的视频文件："+content);
    tvTime.setText(time);

    int status =msg.getSendStatus();
    if (status == BmobIMSendStatus.SEND_FAILED.getStatus()) {
      ivFailResend.setVisibility(View.VISIBLE);
      mProgressBar.setVisibility(View.GONE);
    } else if (status== BmobIMSendStatus.SENDING.getStatus()) {
      ivFailResend.setVisibility(View.GONE);
      mProgressBar.setVisibility(View.VISIBLE);
    } else {
      ivFailResend.setVisibility(View.GONE);
      mProgressBar.setVisibility(View.GONE);
    }

    tvMessage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击" + msg.getContent(), Toast.LENGTH_SHORT).show();
        if(onRecyclerViewListener!=null){
          onRecyclerViewListener.onItemClick(getAdapterPosition());
        }
      }
    });

    tvMessage.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (onRecyclerViewListener != null) {
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });

    ivAvatar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击" + info.getName() + "的头像", Toast.LENGTH_SHORT).show();
      }
    });

    //重发
    ivFailResend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mConversation.resendMessage(msg, new MessageSendListener() {
          @Override
          public void onStart(BmobIMMessage msg) {
            mProgressBar.setVisibility(View.VISIBLE);
            ivFailResend.setVisibility(View.GONE);
            tvSendStatus.setVisibility(View.INVISIBLE);
          }

          @Override
          public void done(BmobIMMessage msg, BmobException e) {
            if(e==null){
              tvSendStatus.setVisibility(View.VISIBLE);
              tvSendStatus.setText("已发送");
              ivFailResend.setVisibility(View.GONE);
              mProgressBar.setVisibility(View.GONE);
            }else{
              ivFailResend.setVisibility(View.VISIBLE);
              mProgressBar.setVisibility(View.GONE);
              tvSendStatus.setVisibility(View.INVISIBLE);
            }
          }
        });
      }
    });
  }

  /**
   * 是否显示时间
   * @param isShow 是否显示
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
