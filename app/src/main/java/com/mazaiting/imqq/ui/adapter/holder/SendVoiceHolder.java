package com.mazaiting.imqq.ui.adapter.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import cn.bmob.newim.bean.BmobIMAudioMessage;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMSendStatus;
import cn.bmob.newim.bean.BmobIMUserInfo;
import cn.bmob.newim.listener.MessageSendListener;
import cn.bmob.v3.exception.BmobException;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;
import com.mazaiting.imqq.ui.base.BaseViewHolder;
import com.mazaiting.imqq.utils.ImageLoaderFactory;
import com.mazaiting.imqq.utils.TimeUtil;

/**
 * 发送语音类型
 * Created by mazaiting on 2017/9/11.
 */

public class SendVoiceHolder extends BaseViewHolder<BmobIMMessage>{

  @BindView(R.id.tv_time_sent_voice)
  protected TextView tvTime;
  @BindView(R.id.iv_avatar_sent_voice)
  protected ImageView ivAvatar;
  @BindView(R.id.iv_voice_sent_voice)
  protected ImageView ivVoice;
  @BindView(R.id.tv_voice_length_sent_voice)
  protected TextView tvVoiceLength;
  @BindView(R.id.iv_fail_resend_sent_voice)
  protected ImageView ivFailResend;
  @BindView(R.id.tv_send_status_sent_voice)
  protected TextView tvSendStatus;
  @BindView(R.id.progress_load_sent_voice)
  protected ProgressBar mProgressBar;
  BmobIMConversation mConversation;

  public SendVoiceHolder(Context context, ViewGroup root, BmobIMConversation conversation,
      OnRecyclerViewListener listener) {
    super(context, root, R.layout.item_chat_sent_voice, listener);
    this.mConversation = conversation;
  }

  @Override public void bindData(BmobIMMessage msg) {
    //用户信息的获取必须在buildFromDB之前，否则会报错'Entity is detached from DAO context'
    final BmobIMUserInfo info = msg.getBmobIMUserInfo();
    // 加载头像
    ImageLoaderFactory.getLoader().loadAvatar(ivAvatar,info != null ? info.getAvatar() : null, R.mipmap.header);
    // 设置时间
    String time = TimeUtil.getTime(true, msg.getCreateTime());
    tvTime.setText(time);
    //使用buildFromDB方法转化成指定类型的消息
    final BmobIMAudioMessage message = BmobIMAudioMessage.buildFromDB(true,msg);
    // 设置时间长度
    tvVoiceLength.setText(message.getDuration()+"\''");

    int status =message.getSendStatus();
    if (status == BmobIMSendStatus.SEND_FAILED.getStatus()||status == BmobIMSendStatus.UPLOAD_FAILED.getStatus()) {//发送失败/上传失败
      ivFailResend.setVisibility(View.VISIBLE);
      mProgressBar.setVisibility(View.GONE);
      tvSendStatus.setVisibility(View.INVISIBLE);
      tvVoiceLength.setVisibility(View.INVISIBLE);
    } else if (status== BmobIMSendStatus.SENDING.getStatus()) {
      mProgressBar.setVisibility(View.VISIBLE);
      ivFailResend.setVisibility(View.GONE);
      tvSendStatus.setVisibility(View.INVISIBLE);
      tvVoiceLength.setVisibility(View.INVISIBLE);
    } else {//发送成功
      ivFailResend.setVisibility(View.GONE);
      mProgressBar.setVisibility(View.GONE);
      tvSendStatus.setVisibility(View.GONE);
      tvVoiceLength.setVisibility(View.VISIBLE);
    }
    // 设置点击
    ivVoice.setOnClickListener(new NewRecordPlayClickListener(getContext(),message,ivVoice));
    // 设置长点击
    ivVoice.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (onRecyclerViewListener != null) {
          onRecyclerViewListener.onItemLongClick(getAdapterPosition());
        }
        return true;
      }
    });

    // 头像点击
    ivAvatar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(mContext, "点击" + info.getName() + "的头像", Toast.LENGTH_SHORT).show();
      }
    });
    //重发
    ivFailResend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mConversation.resendMessage(message, new MessageSendListener() {
          @Override
          public void onStart(BmobIMMessage msg) {
            mProgressBar.setVisibility(View.VISIBLE);
            ivFailResend.setVisibility(View.GONE);
            tvSendStatus.setVisibility(View.INVISIBLE);
          }

          @Override
          public void done(BmobIMMessage msg, BmobException e) {
            if(e==null){
              tvSendStatus.setVisibility(View.VISIBLE);
              tvSendStatus.setText("已发送");
              ivFailResend.setVisibility(View.GONE);
              mProgressBar.setVisibility(View.GONE);
            }else{
              ivFailResend.setVisibility(View.VISIBLE);
              mProgressBar.setVisibility(View.GONE);
              tvSendStatus.setVisibility(View.INVISIBLE);
            }
          }
        });
      }
    });
  }

  /**
   * 显示时间
   * @param isShow 是否显示
   */
  public void showTime(boolean isShow) {
    tvTime.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }
}
