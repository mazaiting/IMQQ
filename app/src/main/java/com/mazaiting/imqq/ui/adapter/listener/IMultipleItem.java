package com.mazaiting.imqq.ui.adapter.listener;

import java.util.List;

/**
 * Created by Administrator on 2016/5/6.
 */
public interface IMultipleItem<T> {

    /**
     * 多种布局的layout文件
     * @param viewType
     * @return
     */
    int getItemLayoutId(int viewType);

    /**
     * 多种布局类型
     * @param position
     * @param t
     * @return
     */
    int getItemViewType(int position, T t);

    /**
     * 返回布局个数
     * @param list
     * @return
     */
    int getItemCount(List<T> list);
}
