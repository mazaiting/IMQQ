package com.mazaiting.imqq.ui.adapter.listener;

/**
 * 为RecycleView添加点击事件\
 * @author mazaiting
 */
public interface OnRecyclerViewListener {
    void onItemClick(int position);
    boolean onItemLongClick(int position);
}
