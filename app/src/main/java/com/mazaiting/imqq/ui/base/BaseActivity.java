package com.mazaiting.imqq.ui.base;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import butterknife.ButterKnife;
import com.mazaiting.imqq.ui.event.IEvent;
import java.lang.reflect.Field;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * 所有Activity的基类
 * Created by mazaiting on 2017/9/4.
 */

public abstract class BaseActivity extends AppCompatActivity{

  public static final String MESSAGEEVENT = "com.mazaiting.imqq.ui.event.MainMessageEvent";
  public static final String CONSTACTSEVENT = "com.mazaiting.imqq.ui.event.MainContactsEvent";
  public static final String DISCOVERYEVENT = "com.mazaiting.imqq.ui.event.MainDiscoveryEvent";
  public static final String SETTINGSEVENT = "com.mazaiting.imqq.ui.event.MainSettingsEvent";
  public static final String REFRESHEVENT = "com.mazaiting.imqq.ui.event.MainRefreshEvent";

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(getLayoutId());
    ButterKnife.bind(this);
    getIntentData();
    initData();
  }

  /**
   * 初始化数据
   */
  protected abstract void initData();

  /**
   * 获取意图传递的数据
   */
  protected void getIntentData() {}

  /**
   * 获取布局ID
   */
  protected abstract int getLayoutId();

  /**
   * 创建一个Intent，开启新的界面
   * @param clazz 类实例
   */
  public void newIntent(Class<?> clazz, Bundle bundle) {
    Intent intent = new Intent(this, clazz);
    if (bundle != null){
      intent.putExtras(bundle);
    }
    startActivity(intent);
  }

  /**
   * 设置沉浸式状态栏
   *
   * @param linear_bar 自定义的状态栏
   */
  protected void setStatusBar(final ViewGroup linear_bar) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      //透明状态栏
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
      //透明导航栏
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
      final int statusHeight = getStatusBarHeight();
      linear_bar.post(new Runnable() {
        @Override
        public void run() {
          int titleHeight = linear_bar.getHeight();
          android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) linear_bar.getLayoutParams();
          params.height = statusHeight + titleHeight;
          linear_bar.setLayoutParams(params);
        }
      });
    }
  }

  /**
   * 获取状态栏的高度
   * @return
   */
  public int getStatusBarHeight() {
    try {
      Class<?> c = Class.forName("com.android.internal.R$dimen");
      Object obj = c.newInstance();
      Field field = c.getField("status_bar_height");
      int x = Integer.parseInt(field.get(obj).toString());
      return getResources().getDimensionPixelSize(x);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return 0;
  }

  /**
   * 隐藏软键盘
   */
  public void hideSoftInputView() {
    InputMethodManager manager = ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE));
    if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
      if (getCurrentFocus() != null)
        manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
  }

  /**隐藏软键盘-一般是EditText.getWindowToken()
   * @param token
   */
  public void hideSoftInput(IBinder token) {
    if (token != null) {
      InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      im.hideSoftInputFromWindow(token,InputMethodManager.HIDE_NOT_ALWAYS);
    }
  }

  @Override protected void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
  }

  @Override protected void onStop() {
    super.onStop();
    EventBus.getDefault().unregister(this);
  }

  @Subscribe
  public void onEvent(IEvent event) {
    switch (event.getClass().getName()){
      case MESSAGEEVENT:
        onMessage();
        break;
      case CONSTACTSEVENT:
        onContact();
        break;
      case DISCOVERYEVENT:
        onDiscovery();
        break;
      case SETTINGSEVENT:
        onSettings();
        break;
      case REFRESHEVENT:
        onRefresh();
        break;
    }
  }

  /**
   * 消息页面处理
   */
  protected void onMessage() {

  }

  /**
   * 联系人页面处理
   */
  protected void onContact() {

  }

  /**
   * 发现页面处理
   */
  protected void onDiscovery() {

  }

  /**
   * 设置页面处理
   */
  protected void onSettings() {

  }

  /**
   * 刷新处理
   */
  protected void onRefresh() {

  }
}
