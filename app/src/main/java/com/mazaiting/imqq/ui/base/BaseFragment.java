package com.mazaiting.imqq.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import com.mazaiting.imqq.ui.event.IEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Fragment基类
 * Created by mazaiting on 2017/9/6.
 */

public abstract class BaseFragment extends Fragment{
  public static final int MESSAGE = 0x00;
  public static final int CONTACTS = 0x01;
  public static final int DISCOVERY = 0x02;
  public static final int SETTINGS = 0x03;

  public static final String MESSAGEEVENT = "com.mazaiting.imqq.ui.event.MainMessageEvent";
  public static final String CONSTACTSEVENT = "com.mazaiting.imqq.ui.event.MainContactsEvent";
  public static final String DISCOVERYEVENT = "com.mazaiting.imqq.ui.event.MainDiscoveryEvent";
  public static final String SETTINGSEVENT = "com.mazaiting.imqq.ui.event.MainSettingsEvent";
  public static final String REFRESHEVENT = "com.mazaiting.imqq.ui.event.MainRefreshEvent";

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(getFragmentLayoutId(), container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
    initData();
  }

  @Override public void onStop() {
    super.onStop();
    EventBus.getDefault().unregister(this);
  }

  @Subscribe
  public void onEvent(IEvent event) {
    switch (event.getClass().getName()){
      case MESSAGEEVENT:
        onMessage();
        break;
      case CONSTACTSEVENT:
        onContact();
        break;
      case DISCOVERYEVENT:
        onDiscovery();
        break;
      case SETTINGSEVENT:
        onSettings();
        break;
      case REFRESHEVENT:
        onRefresh();
        break;
    }
  }

  /**
   * 消息页面处理
   */
  protected void onMessage() {}

  /**
   * 联系人页面处理
   */
  protected void onContact() {}

  /**
   * 发现页面处理
   */
  protected void onDiscovery() {}

  /**
   * 设置页面处理
   */
  protected void onSettings() {}

  /**
   * 刷新处理
   */
  protected void onRefresh() {}

  /**
   * 开启一个新的界面
   * @param clazz
   * @param bundle
   */
  protected void startActivityFragment(Class<?> clazz, Bundle bundle){
    Intent intent = new Intent(getActivity(), clazz);
    if (bundle != null){
      intent.putExtras(bundle);
    }
    startActivity(intent);
  }

  /**
   * 初始化数据
   */
  protected abstract void initData();

  /**
   * 获取布局Id
   * @return
   */
  protected abstract int getFragmentLayoutId();
}
