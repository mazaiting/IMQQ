package com.mazaiting.imqq.ui.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import com.mazaiting.imqq.ui.adapter.listener.OnRecyclerViewListener;

/**
 * Created by mazaiting on 2017/9/6.
 */

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener {
  protected OnRecyclerViewListener onRecyclerViewListener;
  protected Context mContext;

  public BaseViewHolder(Context context, ViewGroup root, int layoutRes, OnRecyclerViewListener listener) {
    super(LayoutInflater.from(context).inflate(layoutRes, root, false));
    this.mContext = context;
    ButterKnife.bind(this, itemView);
    itemView.setOnClickListener(this);
    itemView.setOnLongClickListener(this);
  }

  /**
   * 绑定数据
   * @param t
   */
  public abstract void bindData(T t);

  public Context getContext() {
    return itemView.getContext();
  }

  @Override
  public void onClick(View v) {
    if(onRecyclerViewListener!=null){
      onRecyclerViewListener.onItemClick(getAdapterPosition());
    }
  }

  @Override
  public boolean onLongClick(View v) {
    if(onRecyclerViewListener!=null){
      onRecyclerViewListener.onItemLongClick(getAdapterPosition());
    }
    return true;
  }
}
