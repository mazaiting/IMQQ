package com.mazaiting.imqq.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.ui.base.BaseFragment;

/**
 * 发现
 * A simple {@link Fragment} subclass.
 */
public class DiscoveryFragment extends BaseFragment {

  public DiscoveryFragment() {
    // Required empty public constructor
  }

  @Override protected void initData() {

  }

  @Override protected int getFragmentLayoutId() {
    return R.layout.fragment_discovery;
  }
}
