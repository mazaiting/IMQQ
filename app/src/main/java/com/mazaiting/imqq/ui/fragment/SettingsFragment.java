package com.mazaiting.imqq.ui.fragment;

import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.mazaiting.imqq.R;
import com.mazaiting.imqq.model.bean.UserInfo;
import com.mazaiting.imqq.presenter.user.UserManager;
import com.mazaiting.imqq.ui.base.BaseFragment;
import com.mazaiting.imqq.utils.ImageLoaderFactory;

/**
 * 设置
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends BaseFragment {
  /**
   * 包裹头像的布局
   */
  @BindView(R.id.llMyInfo_settings_fragment)
  LinearLayout llMyInfo;
  /**
   * 头像
   */
  @BindView(R.id.ivHeader_settings_fragment)
  ImageView ivHeader;
  /**
   * 昵称
   */
  @BindView(R.id.tvName_settings_fragment)
  TextView tvName;
  /**
   * 个性签名
   */
  @BindView(R.id.tvAccount_settings_fragment)
  TextView tvAccount;
  /**
   * 二维码
   */
  @BindView(R.id.ivQRCordCard_settings_fragment)
  ImageView ivQRCordCard;

  public SettingsFragment() {
    // Required empty public constructor
  }

  @Override protected void initData() {
    UserInfo currentUser = UserManager.getInstance().getCurrentUser();// 获取当前用户
    tvName.setText(currentUser.getUsername()); // 设置昵称
    // TODO
    tvAccount.setText("没有个性不签名"); // 设置个性签名
    ImageLoaderFactory.getLoader().loadAvatar(ivHeader, currentUser.getAvatar(), R.mipmap.header);// 设置头像
    ImageLoaderFactory.getLoader().loadAvatar(ivQRCordCard, null, R.mipmap.ic_qr_code); // 设置二维码
  }

  @Override protected int getFragmentLayoutId() {
    return R.layout.fragment_settings;
  }
}
