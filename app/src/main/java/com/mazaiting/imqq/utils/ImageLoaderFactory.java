package com.mazaiting.imqq.utils;

/**
 * Created by mazaiting on 2017/9/6.
 */
public class ImageLoaderFactory {

  private static volatile ILoader sInstance;

  private ImageLoaderFactory() {}

  public static ILoader getLoader() {
    if (sInstance == null) {
      synchronized (ImageLoaderFactory.class) {
        if (sInstance == null) {
          sInstance = new UniversalImageLoader();
        }
      }
    }
    return sInstance;
  }
}
