package com.mazaiting.imqq.utils;

import android.app.ActivityManager;
import android.content.Context;
import java.util.Iterator;
import java.util.List;

/**
 * 服务工具类
 * Created by mazaiting on 2017/9/8.
 */

public class ServiceUtil {

  /**
   * 用来判断服务是否运行.
   *
   * @param ctx       the ctx
   * @param className 判断的服务名字 "com.xxx.xx..XXXService"
   * @return true 在运行 false 不在运行
   */
  public static boolean isServiceRunning(Context ctx, String className) {
    boolean isRunning = false;
    ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
    List<ActivityManager.RunningServiceInfo> servicesList = activityManager.getRunningServices(Integer.MAX_VALUE);
    Iterator<ActivityManager.RunningServiceInfo> iterator = servicesList.iterator();
    while (iterator.hasNext()) {
      ActivityManager.RunningServiceInfo si = iterator.next();
      if (className.equals(si.service.getClassName())) {
        isRunning = true;
      }
    }
    return isRunning;
  }

}
